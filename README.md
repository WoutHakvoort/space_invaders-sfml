# Space Invaders - SFML Edition
Made by: Wout Hakvoort

## 1. Build
1. ~: cd build
2. ~: cmake ../
3. ~: make -j4
4. ~: ./../bin/space_invaders

## 2. SFML 2.5.1.
This project uses SFML 2.5.1. This was installed using the following guide:
https://medium.com/@Rewieer/install-sfml-2-5-1-on-ubuntu-18-04-and-clion-9e0dfe86e87f
